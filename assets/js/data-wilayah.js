
var tpl = `
  <table id="bootstrap-data-table" class="table table-striped table-bordered">
   <thead>
     <tr>
       <th>No</th>
       <th>Nama Wilayah</th>
       <th>Area</th>
       <th>Kepala Wilayah</th>
       <th>Lokasi</th>
     </tr>
   </thead>
   <tbody>    
    <tr v-for="wilayah in wilayahList" :key="wilayah.id">
        <td>{{ wilayah.id }}</td>
        <td>{{ wilayah.nama_wilayah }}</td>
        <td>{{ wilayah.area_wilayah }}</td>
        <td>{{ wilayah.kepala_wilayah }}</td>
        <td>{{ wilayah.lokasi_wilayah }}</td>              
    </tr>
    </tbody>
</table>`

Vue.component('wilayah-rows', { template: tpl })

var data_wilayah = new Vue({
  el: '#wilayah',
  data: {
    wilayahList: ''
  },
  template: tpl
})

axios.get(location.origin + "/wilayah", { 
            headers: {
                      "Accept": "application/json",
                      "Content-Type": "application/json",
                      "X-Roles": "admin",
                      "Authorization": "Bearer " + token,                      
                      },
            crossBrowser: true
}).then(function (response) {
            data_wilayah.wilayahList = response.data.wilayah;            
        }
    );