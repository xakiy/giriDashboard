
var tpl = `
  <table id="bootstrap-data-table" class="table table-striped table-bordered">
   <thead>
     <tr>
       <th>No</th>
       <th>Nama</th>
       <th>NIS</th>
       <th>Tempat Lahir</th>
       <th>Tanggal Lahir</th>
       <th>Gender</th>
     </tr>
   </thead>
   <tbody>    
    <tr v-for="(santri, index) in santriList" :key="index">
        <td>{{ index + 1 }}</td>
        <td>{{ santri.nama }}</td>
        <td>{{ santri.nis }}</td>
        <td>{{ santri.tempat_lahir }}</td>
        <td>{{ santri.tanggal_lahir }}</td>
        <td>{{ santri.jenis_kelamin }}</td>
    </tr>
    </tbody>
</table>`

Vue.component('santri-rows', { template: tpl })

var data_santri = new Vue({
  el: '#santri',
  data: {
    santriList: ''
  },
  template: tpl
})

axios.get(location.origin + "/santri", { 
            headers: {
                      "Accept": "application/json",
                      "Content-Type": "application/json",
                      "Authorization": "Bearer " + token
                      },
            crossBrowser: true
}).then(function (response) {
            data_santri.santriList = response.data.santri;
        }
    );